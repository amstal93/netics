const fetch = require('node-fetch')

module.exports = login = async (user) => {
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:4000/api/auth/login`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                },
                body: JSON.stringify({ username: user.username, password: user.password }),
            }).then(res => resolve(res))
            .catch(error => reject(error))
    })

}