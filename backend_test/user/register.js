const fetch = require('node-fetch')

module.exports = register = async (user) => {
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:4000/api/auth/register`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                },
                body: JSON.stringify(user),
            }).then(res => resolve(res))
            .catch(error => reject(error))
    })

}