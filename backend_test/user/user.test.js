const Sentencer = require('sentencer');
const register = require('./register');
const login = require('./login');
const users = require('../users.json')
const createPost = require('./createPost')

const testSentence = "As an avid enthusiast of {{ noun }}, I hereby declare myself {{ adjective }}"

const accessTokens = ''

users.valid.forEach((user, i) => {

    test('Register valid user', async () => {
        const resp = await register(user)
        expect(resp.status).toBe(200)
    });

    test('Login user', async () => {
        const resp = await login(user)
        expect(resp.status).toBe(200)
        const json = await resp.json()
        accessToken = json.tokens.accessToken
    });

    for (let i = 0; i < 10; i++) {
        test(`Create post ${++i}`, async () => {
            const resp = await createPost(user, Sentencer.make(testSentence), accessToken)
            expect(resp.status).toBe(200)
        });
    }
});


test('Register valid user w/ same email', async () => {
    const resp = await register(users.valid[0])
    expect(resp.status).toBe(400)
});



test('Register valid user w/ same email', async () => {
    const resp = await register(users.valid[0])
    expect(resp.status).toBe(400)
});

users.invalid.forEach(user => {

    test('Register invalid user', async () => {
        const resp = await register(users)
        expect(resp.status).toBe(400)
    });

    test('Login invalid credentials', async () => {
        const invalidPw = { ...user, password: "wrong password" }
        const resp = await login(invalidPw)
        expect(resp.status).toBe(401)
    });

});
