const fetch = require('node-fetch')
module.exports = createPost = async (user, postText, accessToken) => {
    const post = {
        postType: "post",
        content: {
            text: postText,
            image: ''
        },
        comments: [],
        hearts: [],
        shares: [],
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        profileImage: user.image
    }
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:4000/api/v1/post`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${accessToken}`
                },
                body: JSON.stringify(post),
            }).then(res => resolve(res))
            .catch(error => reject(error))
    })

}