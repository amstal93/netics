const express = require("express");
const router = express.Router();
const mongoose = require('mongoose')
const decodeToken = require('../app').decodeToken

let message = mongoose.model('msgThread', { from: String, to: String, timestamp: String, content: Object, seen: Boolean })

const mongoDB = process.env.DB || 'mongodb://mongodb:27017/dev'



module.exports = router;