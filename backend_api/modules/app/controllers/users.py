import os
import json
import re
from flask import request, jsonify
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies
)
from datetime import datetime
from app import app, mongo, flask_bcrypt, jwt
from app.schemas.user import validate_user, validate_user_update
from app.schemas.auth import validate_auth
import logger

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


@app.route('/api/auth/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    # Create the new access token
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)

    user = mongo.db.users.find_one({'username': current_user['username']})
    if user and flask_bcrypt.check_password_hash(user['password'], current_user['password']):
        del user['password']
        resp = jsonify({'ok': True, 'data': user, 'tokens': {
                       'accessToken': access_token}})
        return resp, 200


@app.route('/api/auth/login', methods=['POST'])
def auth_user():
    ''' auth endpoint '''
    data = validate_auth(request.get_json(force=True))
    if data['ok']:
        data = data['data']
        data['username'] = data['username']
        user = mongo.db.users.find_one(
            {'username': {'$regex': "(?i)" + data['username'] + "(?i)"}})
        if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
            del user['password']
            access_token = create_access_token(
                identity={'username': user['username'], 'password': data['password']})
            refresh_token = create_refresh_token(
                identity={'username': user['username'], 'password': data['password']})
            resp = jsonify(
                {'ok': True, 'data': user, 'tokens': {'refreshToken': refresh_token, 'accessToken': access_token}})
            return resp, 200
        else:
            return jsonify({'ok': False, 'message': 'invalid username or password'}), 401
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/api/auth/logout', methods=['POST'])
def logout():
    resp = jsonify({'ok': True})
    return resp, 200


@app.route('/api/auth/register', methods=['POST'])
def register():
    ''' register user endpoint '''
    data = validate_user(request.get_json(force=True))
    if data['ok']:
        data = data['data']
        data['timestamp'] = datetime.utcnow()
        data['saved'] = []  # saved posts
        # checks if email is in use
        email = mongo.db.users.find_one({'email': data['email']})
        if email is not None:
            LOG.info(
                'User was not created, email "{}" is taken'.format(data['email']))
            return jsonify({"ok": False, "message": 'E-mail already in use'}), 400
        username = mongo.db.users.find_one(
            {'username': data['username']})
        if username is not None:
            LOG.info('User was not created, username "{}" is taken'.format(
                data['username']))
            return jsonify({"ok": False, "message": 'Username already in use'}), 400
        data['password'] = flask_bcrypt.generate_password_hash(
            data['password'])
        mongo.db.users.insert_one(data)
        LOG.info('User "{}" created'.format(data['username']))
        return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
    else:
        LOG.info('User was not created, bad parameters: {}'.format(
            data['message']))
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/api/v1/user', methods=['GET', 'DELETE', 'PATCH'])
@jwt_required
def user():
    ''' route read user '''
    if request.method == 'GET':
        query = request.args
        data = mongo.db.users.find_one(query)
        del data['password']
        if data is None:
            return jsonify({'ok': True, 'message': 'No record found'}), 204
        else:
            return jsonify({'ok': True, 'data': data}), 200

    if request.method == 'DELETE':
        data = validate_user(request.get_json(force=True))
        if data.get('username', None) is not None:
            db_response = mongo.db.users.delete_one(
                {'username': data['username']})
            if db_response.deleted_count == 1:
                response = {'ok': True, 'message': 'record deleted'}
            else:
                response = {'ok': True, 'message': 'no record found'}
            return jsonify(response), 200
        else:
            LOG.info('User was not deleted, bad parameters: {}'.format(
                data['message']))
            return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400

    if request.method == 'PATCH':
        data = validate_user_update(request.get_json(force=True))
        current_user = get_jwt_identity()
        if data['ok']:
            data = data['data']
            mongo.db.users.update_one(
                {'username': current_user['username']}, {'$set': data})
            user = mongo.db.users.find_one(
                {'username': current_user['username']})
            del user['password']
            return jsonify({'ok': True, 'message': 'record updated', 'data': user}), 200
        else:
            LOG.info('User was not updated, bad parameters: {}'.format(
                data['message']))
            return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/api/v1/follow', methods=['POST'])
@jwt_required
def follow():
    current_user = get_jwt_identity()
    mongo.db.users.update_one({'username': request.args.get('user')}, {
                              '$addToSet': {'followers': current_user['username']}})
    mongo.db.users.update_one({'username': current_user['username']}, {
                              '$addToSet': {'following': request.args.get('user')}})
    return jsonify({'ok': True}), 200


@app.route('/api/v1/unfollow', methods=['POST'])
@jwt_required
def unfollow():
    current_user = get_jwt_identity()
    mongo.db.users.update_one({'username': request.args.get('user')}, {
                              '$pull': {'followers': current_user['username']}})
    mongo.db.users.update_one({'username': current_user['username']}, {
                              '$pull': {'following': request.args.get('user')}})
    return jsonify({'ok': True}), 200


@app.route('/api/v1/usernames', methods=['GET'])
@jwt_required
def getUsernames():
    data = mongo.db.users.find({'username': {
                               '$regex': ".*(?i)" + request.args.get('username') + ".*(?i)"}}, {'username': 1}).limit(5)
    usernames = []
    for doc in data:
        usernames.append(doc['username'])
    resp = jsonify({'ok': True, 'data': usernames})
    return resp, 200
