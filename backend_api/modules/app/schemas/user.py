from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError

user_schema = {
    "type": "object",
    "properties": {
        "_id": {
            "type": "string"
        },
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "email": {
            "type": "string",
            "format": "email"
        },
        "username": {
            "type": "string"
        },
        "password": {
            "type": "string"
        },
        "image": {
            "type": "string"
        },
        "bio": {
            "type": "string"
        },
        "following": {
            "type": "array"
        },
        "followers": {
            "type": "array"
        }
    },
    "required": ["email", "password", "username", "firstName", "lastName"],
    "additionalProperties": False
}

user_update_schema = {
    "type": "object",
    "properties": {
        "_id": {
            "type": "string"
        },
        "timestamp": {
            "type": "string"
        },
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "email": {
            "type": "string",
            "format": "email"
        },
        "username": {
            "type": "string"
        },
        "password": {
            "type": "string"
        },
        "image": {
            "type": "string"
        },
        "bio": {
            "type": "string"
        }
    },
    "required": ["username"],
    "additionalProperties": False
}


def validate_user_update(data):
    try:
        validate(data, user_update_schema)
    except ValidationError as e:
        return {'ok': False, "message": e}
    except SchemaError as e:
        return {'ok': False, "message": e}
    return {'ok': True, "data": data}


def validate_user(data):
    try:
        validate(data, user_schema)
    except ValidationError as e:
        return {'ok': False, "message": e}
    except SchemaError as e:
        return {'ok': False, "message": e}
    return {'ok': True, "data": data}
