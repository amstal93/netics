from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError

message_schema = {
    "type": "object",
    "properties": {
        "_id": {
            "type": "string"
        },
        "sender": {
            "type": "string"
        },
        "recipient": {
            "type": "string"
        },
        "read": {
            "type": "boolean"
        },
        "text": {
            "type": "string"
        },
        "image": {
            "type": "string"
        }
    },
    "required": ["text"],
    "additionalProperties": False
}


def validate_message(data):
    try:
        validate(data, message_schema)
    except ValidationError as e:
        return {'ok': False, "message": e}
    except SchemaError as e:
        return {'ok': False, "message": e}
    return {'ok': True, "data": data}
