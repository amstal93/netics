## Frontend 
- ~~Login form~~
- ~~Register form~~
- ~~Responsive layout~~
- ~~Post tweets~~
- ~~Commenting~~
- ~~Likes/dislikes~~
- ~~Follow/unfollow~~
- ~~Profile pages~~
- Bookmarks (privately saved posts)
- Lists (public organized posts)
- Chat (https://medium.com/javascript-in-plain-english/simple-real-time-web-socket-app-using-node-express-socket-io-react-da73a2685b6a)
- Settings

## Backend
- Routes for the various actions
- Add and handle popularity score
- Serve posts based on follows and popularity
- Docker service for aggregating top lists 
- Chat functionality