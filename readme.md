# Chirper <img src="./frontend/src/assets/Logo_30px.png">


## Deploy application:

1. Install Docker

2. Navigate to project root directory and type:
   ``docker-compose up --build``
3. In a separate terminal window navigate to the **frontend** folder and run:
   ``yarn start``
   > Node-sass library is currently incompatible with latest alpine version

## Frameworks & libraries

### Frontend (Javascript)

- React.js
- Ant.design (UI library)
- Redux (state container)
- Redux thunk (redux functions instead of actions)
- React Router (organize pages and page params)
- History (navigate pages without reloading)
- Node SASS (extended stylesheets)
- HTTP proxy middleware (proxy for avoiding CORS)
- React loading skeleton (pretty loading)

### Backend (Py3.6 & Node.js)

- **REST API (Py3.6)**:
  - Flask (web framework)
  - Flask-JWT-Extended (JWT)
  - Flask-BCrypt (encryption)
  - PyMongo (mongoDB client)
  - JSON-Schema (data validation)
- **Database**:
  - MongoDB (stores data)
- **Message service (Node.js)**:
  - Express (API)
  - Mongoose (mongoDB client and data validation)
  - Socket.IO (websocket)
  - Socket.IO-JWT (socket auth)
  - Nodemon (hot reload development)

### Testing (Node.js)

- Jest (test framework)
- Node-fetch (rest calls)
- MongoDB (deleting DB after tests)
