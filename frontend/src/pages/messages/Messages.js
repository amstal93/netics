import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Row, Col, List } from 'antd'
import home from '../../style/Home.module.css'
import Messenger from '../../components/messages/Messenger';
import InboxLoading from '../../components/messages/InboxLoading';
import Inbox from '../../components/messages/Inbox';
import history from '../../helpers/history';
import MessagesLoading from '../../components/messages/MessagesLoading'

const Messages = (props) => {

    const [] = useState(false)
    const { match, match: { params }, currentUser, message } = props

    useEffect(() => {
    }, [])


    const styles = {
        inboxCol: {
            xs: { span: 9 },
            md: { span: 7 },
            lg: { span: 7 },
            xl: { span: 7 },
        },
        msgCol: {
            xs: { span: 15 },
            md: { span: 17 },
            lg: { span: 17 },
            xl: { span: 17 },
        },
        col: {
            height: '100%',
            minHeight: '100vh'
        },
        inbox: {
            borderRight: '1px solid #ddd',
        }
    }

    const getInbox = () => {
        let index = 0
        message.inbox.forEach((chat, i) => {
            if (params.username !== undefined && chat.users.includes(params.username)) {
                index = i
            }
        })
        return index
    }

    return (
        <div>
            <Row>
                <Col {...styles.inboxCol} style={{ ...styles.col, ...styles.inbox }}>
                    <div className={home.header}>
                        <h1 className={home.title} style={{ fontSize: 24 }}><b>Inbox</b></h1>
                    </div>
                    <div>
                        {
                            message.pending || currentUser.user === null ? <InboxLoading /> : <Inbox params={params} />
                        }
                    </div>
                </Col>
                <Col {...styles.msgCol} style={{ ...styles.col }}>
                    {
                        message.pending || currentUser.user === null ? <MessagesLoading /> : <Messenger params={params} chat={message.inbox[getInbox()]} />
                    }

                </Col>
            </Row>
        </div>
    );
}


const mapStateToProps = state => ({
    currentUser: state.currentUser,
    message: state.message,
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Messages)