import React from 'react'
import {
    Result,
    Button
} from 'antd';
import history from '../../helpers/history'

const NotImplemented = () => {


    return (
        <Result
            style={{
                height: '70vh',
                marginTop: '30vh'
            }}
            status="error"
            title="This page has not been implemented yet."
            extra={
                <Button type="primary" key="console" onClick={() => history.goBack()}>
                    Take me back!
                </Button>
            }
        />
    );
}


export default NotImplemented