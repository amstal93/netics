import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, Modal } from 'antd'
import { postOpen, postCursor } from '../../store/actions/postActions'
import followUser from '../../store/api/user/followUser'
import unfollowUser from '../../store/api/user/unfollowUser'
import getUser from '../../store/api/user/getUser'
import getPosts from '../../store/api/post/getPosts';
import home from '../../style/Home.module.css'
import profile from '../../style/Profile.module.css'
import Feed from '../../components/post/Feed';
import ProfileImage from '../../components/profile/ProfileImage';
import Loading from '../../components/layout/Loading'
import history from '../../helpers/history';
import CreatePost from '../../components/post/CreatePost'
import createPost from '../../store/api/post/createPost'
import createChat from '../../store/api/chat/createChat'
import logoutUser from '../../store/api/auth/logoutUser'
import ProfileLoading from '../../components/profile/ProfileLoading'

const Profile = (props) => {

    const { match: { params }, user, currentUser, message: { inbox } } = props;
    let [page, setPage] = useState(0)
    const [modal, setModal] = useState(false)
    const [msgModal, setMsgModal] = useState(false)
    const [content, setContent] = useState('')

    /**
     * Fetches latest posts of user and user.
     * Adds scroll listener for infinite scroll
     */
    useEffect(() => {
        fetchPosts()
        props.getUser(`username=${params.username}`)
        window.addEventListener('scroll', infiteScroll);
        return () => window.removeEventListener('scroll', infiteScroll);
    }, [])

    const handlePost = (data) => {
        props.createPost(data)
        onCancel()
    }

    const createChat = (obj) => {
        props.createChat(params.username, obj.content)
    }

    const onCancel = () => {
        setModal(false)
        setMsgModal(false)
    }

    const sendMsg = () => {
        const exists = inbox.some(chat => chat.users.includes(params.username))
        if (exists) {
            history.push(`/messages/${params.username}`)
        } else {
            setMsgModal(true)
        }
    }

    /**
     * Fetches posts with user filter
     */
    const fetchPosts = () => {
        props.getPosts(`post?page=${page}&username=${params.username}`, page)
    }

    /**
     * fetches next page if scrolled to bottom
     */
    const infiteScroll = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
            setPage(++page)
            props.getPosts(`post?page=${page}&username=${params.username}`, page)
        }
    }

    /**
     * 
     * @param {event} e
     * opens modal with the username tag in it
     */
    const openPost = (e) => {
        const username = ` @${user.profile.username}`
        setContent(username)
        setModal(true)
    }

    return (
        <div>
            <Modal
                visible={modal}
                onCancel={() => onCancel()}
                footer={<div style={{ height: 20 }} />}
            >
                <CreatePost mentions={true} initContent={content} createPost={handlePost} />
            </Modal >
            <Modal
                visible={msgModal}
                onCancel={() => onCancel()}
                footer={<div style={{ height: 20 }} />}
            >
                <CreatePost
                    placeholder={`Send a message to ${params.username}`}
                    createPost={createChat}
                    charLimit={1024}
                    hideProgress={true}
                    mainButtonText="Send"
                />
            </Modal >
            <div className={home.header}>
                <h1 className={home.title} style={{ fontSize: 24 }}><b>@{params.username}</b></h1>
            </div>
            {
                (!user.profile || !currentUser.user) ?
                    <ProfileLoading /> : // Loadscreen when viewed profile and currentuser is undefined
                    <div className={profile.container}>
                        <div style={{ float: 'right', flexDirection: 'column', width: '100%' }}>
                            <div className={profile.horizontal}>
                                <ProfileImage className={profile.image} src={user.profile.image} />
                                <div className={profile.vertical} style={{ marginLeft: 10 }}>
                                    <h3 className={profile.name}>{`${user.profile.firstName} ${user.profile.lastName}`}</h3>
                                    <h3 className={profile.bio}>{user.profile.bio}</h3>
                                </div>
                            </div>
                            <div className={profile.horizontal} style={{ justifyContent: 'space-between', alignItems: 'center', marginTop: 10 }}>
                                {
                                    params.username !== currentUser.user.username ? <div>
                                        <Button
                                            className={profile.button}
                                            type="primary"
                                            shape="circle"
                                            icon="edit"
                                            onClick={openPost} />
                                        <Button
                                            className={profile.button}
                                            type="primary"
                                            shape="circle"
                                            icon="message"
                                            onClick={() => sendMsg()} />
                                        {
                                            props.currentUser.user.following.includes(params.username) ?
                                                <Button
                                                    className={profile.button}
                                                    type="primary"
                                                    shape="round"
                                                    onClick={() => props.unfollowUser(currentUser.user.username, params.username)}
                                                    icon="user-delete">Unfollow</Button>
                                                :
                                                <Button
                                                    className={profile.button}
                                                    type="primary"
                                                    shape="round"
                                                    onClick={() => props.followUser(currentUser.user.username, params.username)}
                                                    icon="user-add">Follow</Button>
                                        }
                                    </div> :
                                        <div>
                                            <Button
                                                className={profile.button}
                                                type="primary"
                                                shape="circle"
                                                icon="setting"
                                                onClick={() => history.push('/settings')} />
                                            <Button
                                                className={profile.button}
                                                type="primary"
                                                shape="circle"
                                                icon="logout"
                                                onClick={() => props.logoutUser()} />
                                        </div>
                                }
                                <div>
                                    <h3 style={{ margin: '0px 0px 0px 0px' }}>
                                        <b>{user.profile.following.length}</b> following&nbsp;&nbsp;
                                            <b>{user.profile.followers.length}</b> followers
                                        </h3>
                                </div>
                            </div>
                        </div>
                    </div>
            }
            <Feed />
        </div>
    );
}


const mapStateToProps = state => ({
    currentUser: state.currentUser,
    user: state.user,
    post: state.post,
    message: state.message,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    getUser: getUser,
    getPosts: getPosts,
    postOpen: postOpen,
    postCursor: postCursor,
    followUser: followUser,
    unfollowUser: unfollowUser,
    createPost: createPost,
    createChat: createChat,
    logoutUser: logoutUser
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile)