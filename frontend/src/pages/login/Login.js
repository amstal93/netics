import React, { useEffect } from 'react'
import { Form, Icon, Input, Button, Card } from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import loginUser from '../../store/api/auth/loginUser'
import history from '../../helpers/history';
import form from '../../style/Forms.module.css'

const styles = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100vh'
}

const Login = (props) => {

  useEffect(() => {
    if (props.currentUser.user !== null) {
      history.push('/')
    }
  }, [])


  const handleSubmit = event => {
    event.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        props.loginUser(values)
      }
    });
  };

  const { getFieldDecorator } = props.form;
  return (
    <div className={form.background} style={styles}>
      <Card className={form.card} title={<h1 className={form.title}>Sign in</h1>} bordered={false} style={{ width: 300 }}>
        <Form onSubmit={handleSubmit} className={form.loginForm}>
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Username"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Password"
              />,
            )}
          </Form.Item>
          <Form.Item>
            <Button shape="round" type="primary" htmlType="submit" className={form.loginFormButton}>
              <b>Sign in</b>
            </Button>
            &nbsp;&nbsp;Or <a onClick={() => history.push('/register')}>register here!</a>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}

const mapStateToProps = state => ({
  currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loginUser: loginUser
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create({ name: 'login' })(Login))