import React, { } from 'react'
import Skeleton from 'react-loading-skeleton';

const InboxLoading = (props) => {

    return (
        <div style={{padding: "5px 5px 0px 5px"}}>
            <Skeleton height="30px" width="100%" />
            <br />
            <br />
            <div>
                <Skeleton height="20px" width="70%" />
                <br />
                <Skeleton width="56%" />
                <br/>
                <Skeleton width="36%" />
            </div>
            <br />
            <br />
            <div>
                <Skeleton height="20px" width="85%" />
                <br />
                <Skeleton width="56%" />
                <br/>
                <Skeleton width="36%" />
            </div>
            <br />
            <br />
            <div>
                <Skeleton height="20px" width="45%" />
                <br />
                <Skeleton width="56%" />
                <br/>
                <Skeleton width="36%" />
            </div>
        </div>
    );
}

export default InboxLoading