import React, { useState, useEffect } from 'react'
import { Mentions, Modal, Tooltip, Icon } from 'antd'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import messenger from '../../style/Messenger.module.css'
import history from '../../helpers/history';
import InboxLoading from './InboxLoading'
import CreatePost from '../post/CreatePost'
import createChat from '../../store/api/chat/createChat'

const { Option } = Mentions

const Inbox = (props) => {

    const { currentUser, message, params } = props
    const [mentions, setMentions] = useState([])
    const [mentionsVal, setMentionsVal] = useState("")
    const [msgModal, setMsgModal] = useState(false)
    const [recipient, setRecipient] = useState("")

    const onCancel = () => {
        setMsgModal(false)
    }

    const createChat = (obj) => {
        props.createChat(recipient, obj.content)
    }

    const onChange = (e) => {
        setMentionsVal(e)
        fetch(`/api/v1/usernames?username=${e}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                setMentions(
                    !!props.currentUser.user.username ?
                        res.data.filter(username => username !== props.currentUser.user.username) :
                        res.data
                )
            })
            .catch(error => {
                console.error('Oopsie, something went wrong')
                setMentions([])
            })
    }

    useEffect(() => {
    }, [])

    const onSelect = (e) => {
        setMentionsVal("")
        if (message.inbox.some(i => i.users.includes(e.value))) {
            history.push(`/messages/${e.value}`)
        } else {
            setRecipient(e.value)
            setMsgModal(true)
        }
    }

    const scrollToBottom = () => {
        const msgs = document.getElementsByClassName(messenger.messages)[0]
        msgs.scrollTop = msgs.scrollHeight
    }

    const emptyInbox = <div style={{ padding: '5px 5px 5px 5px', borderTop: '1px solid #eee' }}>
        <h3>You inbox is empty</h3>
        <p>Use the search field above to find a user to message <Icon type="smile" /></p>
    </div>

    return (
        <div>
            <Modal
                visible={msgModal}
                onCancel={() => onCancel()}
                footer={<div style={{ height: 20 }} />}
            >
                <CreatePost
                    enterSend={true}
                    placeholder={`Send a message to ${recipient}`}
                    createPost={createChat}
                    charLimit={1024}
                    hideProgress={true}
                    mainButtonText="Send"
                />
            </Modal >
            <Mentions
                style={{ width: '100%' }}
                onChange={onChange}
                onSelect={onSelect}
                defaultValue=""
                prefix=""
                value={mentionsVal}
                placeholder="Search for a user"
            >
                {
                    mentions.map(user => <Option key={user} value={user}>
                        {user}
                    </Option>)
                }
            </Mentions>
            {
                message.inbox.length === 0 ? emptyInbox : // todo: replace with help
                    message.inbox.map(chat => {
                        const lastMsg = chat.messages.length - 1
                        return (
                            <div key={chat._id} className={messenger.inbox} onClick={() => history.push(`/messages/${chat.users.filter(u => u !== currentUser.user.username)[0]}`)}>
                                <h3>{chat.users.filter(username => username !== currentUser.user.username).join(', ')}</h3>
                                <p>{chat.messages[lastMsg].text === '' && chat.messages[lastMsg].image.length > 0
                                    ? `${params.username}: Sent a picture` :
                                    `${chat.messages[lastMsg].sender}: ${chat.messages[lastMsg].text}`}</p>
                                <p>{chat.messages[lastMsg].timestamp.split('.')[0]}</p>
                            </div>
                        )
                    })
            }
        </div>
    );
}


const mapStateToProps = state => ({
    currentUser: state.currentUser,
    message: state.message,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createChat: createChat
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Inbox)