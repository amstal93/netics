import React, { } from 'react'
import Skeleton from 'react-loading-skeleton';

const MessagesLoading = (props) => {

    return (
        <div style={{height: '100%', padding: "10px 10px 10px 10px"}}>
            <div>
                <Skeleton height="25px" width="54%" />
                <br />
                <Skeleton height="25px" width="70%" />
                <br />
                <Skeleton height="25px" width="66%" />
            </div>
            <br />
            <br /><div>
                <Skeleton height="25px" width="34%" />
                <br />
                <Skeleton height="25px" width="55%" />
                <br />
                <Skeleton height="25px" width="69%" />
            </div>
            <br />
            <br /><div>
                <Skeleton height="25px" width="23%" />
                <br />
                <Skeleton height="25px" width="70%" />
                <br />
                <Skeleton height="25px" width="10%" />
            </div>
        </div>
    );
}

export default MessagesLoading