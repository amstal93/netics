import React, { useState } from 'react'
import { Row, Col } from 'antd'
import LeftPanel from './toolbar/LeftPanel'
import RightPanel from './search/RightPanel'
import socketIOClient from 'socket.io-client'

const chatPort = process.env.CHAT_PORT || 3001
const chatHost = process.env.CHAT_HOST || 'localhost'
const endpoint = `http://${chatHost}:${chatPort}`
const socket = socketIOClient(endpoint)

const Layout = (props) => {


    const styles = {
        toolsCol: {
            xs: { span: 4 },
            md: { span: 7 },
            lg: { span: 7 },
            xl: { span: 7 },
        },
        childrenCol: {
            xs: { span: 20 },
            md: { span: 10 },
            lg: { span: 10 },
            xl: { span: 10 },
        },
        childrenColExp: {
            xs: { span: 20 },
            md: { span: 17 },
            lg: { span: 17 },
            xl: { span: 17 },
        },
        searchCol: {
            xs: { span: 0 },
            md: { span: 7 },
            lg: { span: 7 },
            xl: { span: 7 },
        },
        col: {
            height: '100%',
            minHeight: '100vh'
        },
        children: {
            borderLeft: '1px solid #ddd',
            borderRight: '1px solid #ddd',
        }
    }
    return (
        <div>
            <Row>
                <Col {...styles.toolsCol} style={{ ...styles.col }}>
                    <div style={{ position: 'fixed', float: 'right', width: 'inherit' }}>
                        <LeftPanel />
                    </div>
                </Col>
                {
                    !props.rightPanel ?
                        <Col {...styles.childrenColExp} style={{ ...styles.col, ...styles.children }}>{props.children}</Col>
                        :
                        <Col {...styles.childrenCol} style={{ ...styles.col, ...styles.children }}>{props.children}</Col>
                }
                {!!props.rightPanel && <Col {...styles.searchCol} style={{ ...styles.col }}>
                    <RightPanel />
                </Col>}
            </Row>
        </div >
    )
}

export default Layout