import React from 'react'
import { Icon, Tooltip, Badge } from 'antd';
import { connect } from 'react-redux'
import cx from 'classnames'
import PostModal from './PostModal'
import '../../../style/Toolbar.module.css'
import { postOpen } from '../../../store/actions/postActions';
import toolbar from '../../../style/Toolbar.module.css'
import post from '../../../style/Post.module.css'
import '../../../style/LeftPanel.style.scss'
import history from '../../../helpers/history';
import logo from '../../../assets/Logo.png'

const Toolbar = (props) => {

    const pushMessages = () => {
        history.push(`/messages`)
    }

    return (
        <div className="panel">
            <div className="desktop">
                <div className={toolbar.container}>
                    <div className={toolbar.button} style={{ margin: '0px 0px 0px 0px !important' }}>
                        <div className={toolbar.children}>
                            <img src={logo} style={{ height: 25 }} />
                        </div>
                    </div>
                    <PostModal />
                    <div onClick={() => history.push('/')} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="home" /><b>Home</b></h2>
                    </div>
                    <div onClick={() => history.push('/discover')} className={toolbar.button}>
                        <h2 className={toolbar.children}>
                            <Icon className={toolbar.icon} type="number" /><b>Discover</b></h2>
                    </div>
                    <div onClick={() => history.push('/notimplemented')} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="bell" /><Badge count={17}><b style={{ fontSize: '1.5em' }}>Notifications</b></Badge></h2>
                    </div>
                    <div onClick={() => pushMessages()} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="message" /><b>Messages</b></h2>
                    </div>
                    <div onClick={() => history.push('/bookmarks')} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="book" /><b>Bookmarks</b></h2>
                    </div>
                    <div onClick={() => history.push('/notimplemented')} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="unordered-list" /><b>Lists</b></h2>
                    </div>
                    <div onClick={() => history.push(`/profile/${props.currentUser.user.username}`)} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="user" /><b>Profile</b></h2>
                    </div>
                    <div onClick={() => history.push('/notimplemented')} className={toolbar.button}>
                        <h2 className={toolbar.children}><Icon className={toolbar.icon} type="more" /><b>More</b></h2>
                    </div>
                    <div className={post.button} onClick={() => props.dispatch(postOpen(''))}>
                        <h2 className={post.Button}>Write a post</h2>
                    </div>
                </div>
            </div>
            <div className="mobile">
                <div className={cx([toolbar.smallContainer, post.centerContainer])}>
                    <PostModal />
                    <div className={toolbar.button} style={{ margin: '0px 0px 0px 0px !important' }}>
                        <div className={toolbar.smallChildren}>
                            <img src={logo} style={{ height: 25 }} />
                        </div>
                    </div>
                    <div onClick={() => history.push('/')} className={toolbar.button}>
                        <Tooltip title="Home" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="home" /></Tooltip>
                    </div>
                    <div onClick={() => history.push('/discover')} className={toolbar.button}>
                        <Tooltip title="Discover" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="number" /></Tooltip>
                    </div>
                    <div onClick={() => history.push('/notimplemented')} className={toolbar.button}>
                        <Tooltip title="Notifications" placement="right" >
                            <Badge count={17} offset={[-20, 20]}>
                                <Icon className={cx([toolbar.smallChildren, toolbar.icon])} style={{ fontSize: '1.5em' }} type="bell" />
                            </Badge>
                        </Tooltip>
                    </div>
                    <div onClick={() => pushMessages()} className={toolbar.button}>
                        <Tooltip title="Messages" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="message" /></Tooltip>
                    </div>
                    <div onClick={() => history.push('/bookmarks')} className={toolbar.button}>
                        <Tooltip title="Bookmarks" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="book" /></Tooltip>
                    </div>
                    <div onClick={() => history.push('/notimplemented')} className={toolbar.button}>
                        <Tooltip title="Lists" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="unordered-list" /></Tooltip>
                    </div>
                    <div onClick={() => history.push(`/profile/${props.currentUser.user.username}`)} className={toolbar.button}>
                        <Tooltip title="Profile" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="user" /></Tooltip>
                    </div>
                    <div onClick={() => history.push('/notimplemented')} className={toolbar.button}>
                        <Tooltip title="More" placement="right" ><Icon className={cx([toolbar.smallChildren, toolbar.icon])} type="more" /></Tooltip>
                    </div>
                    <div className={toolbar.smallPost} onClick={() => props.dispatch(postOpen(''))}>
                        <Icon className={cx([toolbar.icon])} type="edit" />
                    </div>
                </div>
            </div>

        </div >
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser,
    message: state.message,
})

export default connect(
    mapStateToProps
)(Toolbar)