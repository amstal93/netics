import React from 'react'
import { Modal } from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import createPost from '../../../store/api/post/createPost'
import CreatePost from '../../post/CreatePost'
import { postClose } from '../../../store/actions/postActions';

const PostModal = (props) => {

    const handlePost = (data) => {
        props.createPost(data)
        onCancel()
    }

    const onCancel = () => {
        props.postClose()
    }

    return (
        <Modal
            visible={props.post.postOpen}
            onCancel={() => onCancel()}
            footer={<div style={{ height: 20 }} />}
        >
            <CreatePost createPost={handlePost} mentions={true} />

        </Modal >
    );
}

const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createPost: createPost,
    postClose: postClose
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostModal)