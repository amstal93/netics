import React from 'react'
import { Icon } from 'antd'
import post from '../../style/Post.module.css'

const Loading = () => {
    return (
        <div className={post.centerContainer} style={{ width: '100%' }}>
            <Icon style={{ fontSize: 40, color: '#1890ff', padding: '10px 10px 10px 10px' }} type="loading" />
        </div>
    )
}

export default Loading