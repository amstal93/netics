import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, BackTop, notification, Icon } from 'antd'
import { Route, Redirect } from 'react-router-dom'
import Navigation from './Navigation'
import refreshToken from '../../store/api/auth/refreshToken'
import fetchChat from '../../store/api/chat/fetchChat'
import socketIOClient from 'socket.io-client'
import { pushMsgFunc, pushChatFunc, pushUsers } from '../../store/actions/chatActions'
import history from '../../helpers/history'

const chatPort = process.env.CHAT_PORT || 3001
const chatHost = process.env.CHAT_HOST || 'localhost'
const endpoint = `http://${chatHost}:${chatPort}`


const AuthRoute = ({ dispatch, component: Component, refreshToken, fetchChat, ...rest }) => {

    useEffect(() => {
        // If logged in connect to websocket
        refreshToken()
        fetchChat()
        const socket = socketIOClient(endpoint)
        socket
            .on('authenticated', function () {
                //do other things
                const username = JSON.parse(atob(localStorage.getItem('accessToken').split('.')[1])).identity.username
                socket.on(`onlineUsers`, users => {
                    console.log(users)
                    //                dispatch(pushUsers((i)))
                })
                socket.on(socket.id, message => {
                    if (message.new) {
                        rest.pushChatFunc(message)
                        msgNotification(message.messages[0].sender, message.messages[0].text)
                    } else {
                        rest.pushMsgFunc(message)
                        msgNotification(message.sender, message.text)
                    }
                })
            })
            .emit('authenticate', { token: localStorage.getItem('accessToken') });
    }, [])

    const msgNotification = (sender, message) => {
        if (message.length > 50) {
            message = message.substring(0, 50) + "..."
        } else if (!!message.image && !message.text) {
            message = `${sender} sent you a picture!`
        }
        notification.info({
            icon: <Icon theme="twoTone" type="message" />,
            message: `${sender} sent you a message`,
            description: message,
            onClick: () => history.push('/messages/' + sender)
        });
    };

    return <Route {...rest} render={props => (
        localStorage.getItem('accessToken')
            ?
            <div>
                {window.scrollTo(0, 0)}
                <Navigation rightPanel={rest.rightPanel}>
                    <Component {...props} {...rest} />
                </Navigation>
                {
                    !!rest.rightPanel &&
                    <BackTop>
                        <Button
                            style={{ position: 'fixed', bottom: 30, right: 30 }}
                            icon="up"
                            shape="circle"
                            size="large"
                            type="primary" />
                    </BackTop>
                }
            </div>
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
    dispatch: dispatch,
    refreshToken: refreshToken,
    fetchChat: fetchChat,
    pushChatFunc: pushChatFunc,
    pushMsgFunc: pushMsgFunc
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthRoute)