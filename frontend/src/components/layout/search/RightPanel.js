import React from 'react'
import rightPantel from '../../../style/RightPanel.module.css'
import Search from './Search'
import Trending from './Trending'

const RightPanel = (props) => {


    return (
        <div style={{width: '100%'}}>
            <div className={rightPantel.container}>
                <Search/>
                <div /* divider */ style={{height: 20}} />
                <Trending/>
            </div>
        </div>
    );
}

export default RightPanel