import React from 'react'
import post from '../../style/Post.module.css'
import Skeleton from 'react-loading-skeleton';


const FeedLoading = (props) => {

    return (
        <div className={post.post}>
            <div className={post.centerContainer} style={{ flexDirection: 'column' }} >
                <Skeleton circle={true} height={50} width={50} />
                <Skeleton className={post.sideStripe} height={210} width={7.5} />
            </div>
            <div className={post.content}>
                <div className={post.handle}>
                    &nbsp;<Skeleton width="20%" />
                    &nbsp;&nbsp;
                    <Skeleton width="20%" />
                    <div style={{ float: 'right' }}>
                        <Skeleton circle={true} width={20} heigth={20} />
                    </div>
                </div>
                <div style={{ marginLeft: '5px' }}>
                    <Skeleton width="15%" />
                </div>
                <div style={{ marginTop: '10px' }}>
                    <Skeleton height={100} width="100%" />
                </div>
                <Skeleton width="80%" />
                <Skeleton width="45%" />
                <Skeleton width="65%" />
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <div style={{ marginRight: 10 }}>
                        <Skeleton width={39} height={39} circle={true} />
                    </div>
                    <div style={{ marginRight: 10 }}>
                        <Skeleton width={39} height={39} circle={true} />
                    </div>
                    {/*<div style={{ marginRight: 10 }}>
                        <Skeleton width={39} height={39} circle={true} />
                    </div>*/}
                    <Skeleton width={39} height={39} circle={true} />
                </div>
            </div >
            <div>
                <Skeleton className={post.delete} />
            </div>
        </div >
    );
}

export default FeedLoading