
import React from 'react'
import { Modal, Icon, Popover, Button } from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import post from '../../style/Post.module.css'
import ProfileImage from '../profile/ProfileImage'
import CreatePost from './CreatePost'
import createComment from '../../store/api/post/createComment'
import deleteComment from '../../store/api/post/deleteComment'
import timeSince from '../../helpers/timeSince';

const Post = (props) => {

    const { show, close, data, currentUser } = props

    const createComment = (postObj) => {
        const comment = {
            username: currentUser.user.username,
            profileImage: postObj.profileImage,
            content: postObj.content
        }
        props.createComment(data._id, comment)
        // TODO Add redux components, api and backend route
    }

    const footer = !!currentUser.user && <div className={post.comments}>
        {
            (data.comments.length === 0 ? <div className={post.centerContainer}>
                <h3 style={{ color: '#888', paddingTop: 5 }}><Icon type="frown" /> No comments</h3>
            </div> :
                data.comments.map((comment, i) => <div key={i} className={post.comment} style={{ borderBottom: i !== 0 ? '1px solid #ccc' : '', paddingTop: 0 }}>
                    <ProfileImage className={post.profileImage} src={comment.profileImage} />
                    <div>
                        <p className={post.commentName}>{<b>@{comment.username}</b>} {timeSince(comment.timestamp)}</p>
                        <div className={post.commentContent}>
                            <p>{comment.content.text}</p>
                            <img className={post.commentImage} src={comment.content.image} />
                        </div>
                    </div>
                    {comment.username === currentUser.user.username && < Popover
                        placement="right"
                        title="Are you sure?"
                        content={<Button
                            style={{ width: '100%' }}
                            shape="round"
                            type="danger"
                            onClick={() => {
                                props.deleteComment(data._id, comment._id)
                            }}>
                            Delete
                            </Button>
                        } trigger="click">
                        <Icon className={post.delete}
                            style={{ height: 'min-content', marginLeft: 'auto' }} type="delete" />
                    </Popover>}
                </div>).reverse())
        }
    </div >

    return (
        <Modal maskClosable={true} visible={show} onCancel={() => close()} footer={footer}>
            <div style={{ padding: '0px 10px 0px 10px' }} className={post.comment}>
                <div className={post.centerContainer} style={{ flexDirection: 'column' }} >
                    <ProfileImage className={post.profileImage} src={data.profileImage} />
                    <div className={post.sideStripe} />
                </div>
                <div className={post.content}>
                    <p className={post.handle}>
                        <b className={post.name}>{data.firstName} {data.lastName}</b>
                        &nbsp;<a>
                            @{data.username}
                        </a> {timeSince(data.timestamp)}
                    </p>
                    {
                        data.content.text.split('\n').map((line, i) => <p className={post.text} style={{ marginTop: 10 }} key={i}>{
                            line.split(' ')
                                .map(word => word.charAt(0) === "#" ? <a key={i}>{word}</a> :
                                    word.charAt(0) === "@" ? <a key={i} >{word}</a> : word + " ")
                        }</p>
                        )
                    }
                    {data.content.image &&
                        <img className={post.photo} alt="" src={data.content.image} />
                    }
                </div >
            </div>
            <div style={{ padding: '0px 0px 0px 0px', width: '100%' }} className={post.comment}>
                <CreatePost placeholder="Write a comment" mentions={true} disableDivider={true} overridePost={createComment} />
            </div>
        </Modal>
    );
}


const mapStateToProps = state => ({
    app: state.app,
    currentUser: state.currentUser,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createComment: createComment,
    deleteComment: deleteComment
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Post)