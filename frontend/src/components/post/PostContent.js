import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import createPost from '../../store/api/post/createPost'
import deletePost from '../../store/api/post/deletePost'
import heartPost from '../../store/api/post/heartPost'
import unheartPost from '../../store/api/post/unheartPost'
import post from '../../style/Post.module.css'
import history from '../../helpers/history';

const PostContent = (props) => {

    const { data } = props

    return (
        <div>
            {
                data.content.text.split('\n').map((line, i) => <p className={post.text} key={i}>{
                    line.split(' ')
                        .map((word, j) => word.charAt(0) === "#" ? <a key={j}>{word}</a> :
                            word.charAt(0) === "@" ? <a key={j} onClick={() => history.push(`/profile/${word.substring(1, word.length)}`)} >{word}</a> : word + " ")
                }</p>
                )
            }
            {data.content.image &&
                <img className={post.photo} alt="" src={data.content.image} />
            }
        </div >
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createPost: createPost,
    deletePost: deletePost,
    heartPost: heartPost,
    unheartPost: unheartPost
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostContent)