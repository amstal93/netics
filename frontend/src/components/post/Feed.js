import React, { useState, useEffect } from 'react'
import Post from './Post'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import getUser from '../../store/api/user/getUser'
import getPosts from '../../store/api/post/getPosts';
import deletePost from '../../store/api/post/deletePost';
import home from '../../style/Home.module.css'
import post from '../../style/Post.module.css'
import { Icon } from 'antd'
import FeedLoading from './FeedLoading';

const Feed = (props) => {

    const noContent = <div>
        <div className={post.centerContainer} style={{ marginTop: 20 }}>
            <h1 style={{ color: '#777' }}><Icon type="frown" /> No content...</h1>
        </div>
    </div>

    const content = props.post.posts.map((post, key) => <Post
        key={key}
        data={post}
        delete={props.currentUser.user !== null && post.username === props.currentUser.user.username}
    />)

    if (props.error) {
        return noContent
    }

    return (
        <div className={home.feed}>
            {props.divider !== undefined && <div style={{ height: `${props.divider}px` }} />}
            {
                props.post.posts.length > 0 &&
                content
            }
            {props.post.error && noContent}
            {(props.post.pending && !props.post.error) && <div>
                <FeedLoading />
                <FeedLoading />
                <FeedLoading />
            </div>}
        </div>
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    getUser: getUser,
    getPosts: getPosts,
    deletePost: deletePost,
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Feed)