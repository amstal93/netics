import React, { useState } from 'react'
import { Button, Popover, Progress, Upload, Mentions } from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import post from '../../style/Post.module.css'
import ProfileImage from '../profile/ProfileImage';
import AvatarLoading from './AvatarLoading';

const { Option } = Mentions

const CreatePost = (props) => {

    const { currentUser } = props // current user from redux

    const [image, setImage] = useState('') // image is set locally in the component
    const [content, setContent] = useState(props.initContent || '')
    const [cursor, setCursor] = useState(0)
    const [mentions, setMentions] = useState([])

    const onSelect = () => { }

    const maxWords = props.charLimit || 140
    const limit = 400 // Max height for component

    /**
     * Creates post, then sets the post object in redux to blank
     * resets cursor and image aswell
     */
    const handlePost = () => {
        const postObj = {
            postType: "post",
            content: {
                text: content,
                image: image,
            },
            comments: [],
            hearts: [],
            shares: [],
            username: currentUser.user.username,
            firstName: currentUser.user.firstName,
            lastName: currentUser.user.lastName,
            profileImage: currentUser.user.image || ""
        }

        if (!!props.overridePost) {
            props.overridePost(postObj)
        } else {
            props.createPost(postObj)
        }
        setContent('')
        setCursor(0)
        setImage('')
    }

    /**
     * 
     * @param {image file} file Base64 encodes the image before using it in state
     */
    const handleImage = (file) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onloadend = () => {
            setImage(reader.result)
        }
        return false
    }

    const findMentions = (str, itemChar) => {
        const cursor = document.getElementById(post.postText).selectionStart
        for (let i = cursor; i >= 0; i--) {
            if (str.charAt(i) === itemChar) {
                return str.substring(i + 1, cursor)
            }
        }
    }

    /**
     * 
     * @param {event} e 
     * Adjusts height of post input, updates cursor position,
     * and adds input to post
     */
    const handlePostChange = (e) => {
        if (!!props.mentions) {
            let search = findMentions(e, '@')
            if (!!search) {
                fetch(`/api/v1/usernames?username=${search}`,
                    {
                        method: 'GET',
                        headers: {
                            Accept: 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                        },
                    }).then(res => res.json())
                    .then(res => {
                        setMentions(res.data)
                    })
                    .catch(error => {
                        console.error('Oopsie, something went wrong')
                        setMentions([])
                    })
            }
            search = findMentions(e, '#')
            if (search) {
                // TODO: setup endpoint for hashtags
                /*fetch(`/api/v1/usernames?username=${e.substring(1, e.length)}`,
                    {
                        method: 'GET',
                        headers: {
                            Accept: 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                        },
                    }).then(res => res.json())
                    .then(res => {
                        setMentions(res.data)
                    })
                    .catch(error => {
                        console.error('Oopsie, something went wrong')
                        setMentions([])
                    })*/
            }
        }
        if (!!props.enterSend && e.includes('\n') && e.trim().length > 0) {
            handlePost()
        }
        const textArea = document.getElementById(post.postText)
        textArea.style.height = ""
        textArea.style.height = `${Math.min(textArea.scrollHeight, limit)}px`
        setCursor(cursor + e.length - content.length)
        setContent(e)
    }

    /**
     * 
     * @param {event} e 
     * updates cursor position
     */
    const trackCursor = (e) => {
        setCursor(e.target.selectionStart)
    }

    /**
     * 
     * @param {string} emoji inserts an emoji at given position
     */
    const setEmoji = (emoji) => {
        const before = content.substring(0, cursor)
        const after = content.substring(cursor, content.length)
        setContent((before + emoji + after))
        setCursor(cursor + 2)
    }
    const emojiList = [
        {
            category: 'Emojis',
            emojis: ['😃', '😂', '😍', '😎']
        }
    ]

    const emojiPicker = <div style={{ marginTop: 10 }}>
        {emojiList.map((e, index) => (
            <div key={index}>
                <div><b>{e.category}</b></div>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    {e.emojis.map((emoji, i) => (
                        <h1 className={post.emoji} key={i} onClick={() => setEmoji(emoji)}>{emoji}</h1>
                    ))}
                </div>
            </div>
        ))}
    </div>

    return (
        <div style={{ width: 'inherit', paddingBottom: 10 }}>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ marginTop: 5, marginLeft: 10 }}>
                    {currentUser.user === null ?
                        <div style={{ marginRight: 8 }} ><AvatarLoading /></div> :
                        <ProfileImage src={currentUser.user.image} className={post.profileImage} />}
                </div>
                <Mentions
                    onKeyDown={handlePostChange}
                    onClick={trackCursor}
                    value={content}
                    prefix={['@', '#']}
                    maxLength={maxWords}
                    onChange={handlePostChange}
                    id={post.postText}
                    autoFocus={true}
                    placeholder={props.placeholder || "What's on your mind?"}
                >
                    {
                        mentions.map(user => <Option className={post.mentions} key={user} value={user}>
                            {user}
                        </Option>)
                    }
                </Mentions>
                {image &&
                    <div className={post.photoParent}>
                        <img alt="" className={post.photo} style={{ maxWidth: 200 }} src={image} />
                    </div>
                }
            </div>
            {
                !props.hideProgress && <Progress className={post.progress} status='normal' percent={100 * (content.length / maxWords)} showInfo={false} />
            }
            <div className={post.footer}>
                <div style={{ float: 'left', marginTop: 5 }}>
                    {
                        image ? <Button icon='delete' shape='circle' type='danger' onClick={() => setImage(null)} />
                            :
                            <Upload showUploadList={false} accept="image/*" beforeUpload={handleImage}>
                                <Button disabled={image} icon='picture' shape='circle' type='primary' />
                            </Upload>
                    }
                </div>
                <div style={{ float: 'right' }}>
                    <Popover placement='bottom' content={emojiPicker} trigger='click'>
                        <Button icon='smile' shape='circle' type='primary' />
                    </Popover>
                    <Button
                        style={{ marginLeft: 10 }}
                        disabled={!(!!content || !!image)}
                        onClick={() => handlePost()}
                        shape='round'
                        type='primary'
                        size='large'><b>{props.mainButtonText || "Post"}</b></Button>
                </div>
                {
                    !props.disableDivider && <div style={{ width: '100%', height: 10 }} />
                }
            </div>
        </div >
    );
}

const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser,
    app: state.app
})

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreatePost)