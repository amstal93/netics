import React, { useState } from 'react'
import history from '../../helpers/history'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon, Button, Popover } from 'antd'
import createPost from '../../store/api/post/createPost'
import deletePost from '../../store/api/post/deletePost'
import heartPost from '../../store/api/post/heartPost'
import unheartPost from '../../store/api/post/unheartPost'
import ProfileImage from '../profile/ProfileImage'
import post from '../../style/Post.module.css'
import PostContent from './PostContent';
import BottomButtons from './BottomButtons';
import timeSince from '../../helpers/timeSince'

const Post = (props) => {

    const { data } = props
    const [] = useState(false)

    return (
        <div className={post.post}>
            <div className={post.centerContainer} style={{ flexDirection: 'column' }} >
                <ProfileImage className={post.profileImage} src={data.profileImage} />
                <div className={post.sideStripe} />
            </div>
            <div className={post.content}>
                <p className={post.handle}>
                    <b className={post.name}>{data.firstName} {data.lastName}</b>
                    &nbsp;<a onClick={() => history.push(`/profile/${data.username}`)}>
                        @{data.username}
                    </a>
                </p>
                <p>{timeSince(data.timestamp)}</p>
                <PostContent data={data} />
                <BottomButtons data={data} />
            </div >
            <div>
                {
                    props.delete && <Popover
                        placement="right"
                        title="Are you sure?"
                        content={<Button
                            style={{ width: '100%' }}
                            shape="round"
                            type="danger"
                            onClick={() => {
                                props.deletePost(data)
                            }}>
                            Delete
                            </Button>
                        } trigger="click">
                        <Icon className={post.delete} type="delete" />
                    </Popover>
                }
            </div>
        </div >
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createPost: createPost,
    deletePost: deletePost,
    heartPost: heartPost,
    unheartPost: unheartPost
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Post)