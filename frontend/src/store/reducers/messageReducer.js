import {
    SEND_MESSAGE_PENDING,
    SEND_MESSAGE_SUCCESS,
    SEND_MESSAGE_FAILURE,
    FETCH_CHAT_PENDING,
    FETCH_CHAT_SUCCESS,
    FETCH_CHAT_FAILURE,
    CREATE_CHAT_PENDING,
    CREATE_CHAT_SUCCESS,
    CREATE_CHAT_FAILURE,
    PUSH_MSG,
    PUSH_CHAT,
    PUSH_USERS
} from '../actions/chatActions'

const initialState = {
    inbox: [],
    onlineUsers: [],
    pending: false,
    sendPending: false,
    error: null
}

export const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case PUSH_CHAT:
            if (!action.chat) {
                return state
            } else {
                return {
                    ...state,
                    inbox: state.inbox.some(i => i._id === action.chat._id) ? state.inbox : [...state.inbox, action.chat]
                }
            }
        case PUSH_MSG:
            return {
                ...state,
                inbox: state.inbox.map(chat => chat._id === action.message.chatId ? {...chat, messages: [...chat.messages, action.message]} : chat )
            }
        case PUSH_USERS:
            return {
                ...state,
                onlineUsers: action.users
            }
        // Send message
        case SEND_MESSAGE_PENDING:
            return {
                ...state,
                sendPending: true
            }
        case SEND_MESSAGE_SUCCESS:
            return {
                ...state,
                sendPending: false,
                inbox: state.inbox.map(inbox => inbox._id === action.chatId ? { ...inbox, messages: [...inbox.messages, action.message] } : inbox)
            }
        case SEND_MESSAGE_FAILURE:
            return {
                ...state,
                sendPending: false
            }
        // Fetch DMs
        case FETCH_CHAT_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_CHAT_SUCCESS:
            return {
                ...state,
                pending: false,
                inbox: action.data
            }
        case FETCH_CHAT_FAILURE:
            return {
                ...state,
                pending: false
            }
        // Create new DM
        case CREATE_CHAT_PENDING:
            return {
                ...state,
                pending: true
            }
        case CREATE_CHAT_SUCCESS:
            return {
                ...state,
                pending: false,
                inbox: [action.chat, ...state.inbox]
            }
        case CREATE_CHAT_FAILURE:
            return {
                ...state,
                pending: false
            }

        default:
            return state;
    }
}
