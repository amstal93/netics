import {
    POST_WRITE,
    COMMENT_CREATE_PENDING,
    COMMENT_CREATE_SUCCESS,
    COMMENT_CREATE_FAILURE,
    COMMENT_DELETE_PENDING,
    COMMENT_DELETE_SUCCESS,
    COMMENT_DELETE_FAILURE,
    POST_CREATE_PENDING,
    POST_CREATE_SUCCESS,
    POST_CREATE_FAILURE,
    POST_DELETE_PENDING,
    POST_DELETE_SUCCESS,
    POST_DELETE_FAILURE,
    POST_GET_PENDING,
    POST_GET_SUCCESS,
    POST_GET_FAILURE,
    POST_HEART_PENDING,
    POST_HEART_SUCCESS,
    POST_HEART_FAILURE,
    POST_UNHEART_PENDING,
    POST_UNHEART_SUCCESS,
    POST_UNHEART_FAILURE,
    POST_BOOKMARK_PENDING,
    POST_BOOKMARK_SUCCESS,
    POST_BOOKMARK_FAILURE,
    POST_UNBOOKMARK_PENDING,
    POST_UNBOOKMARK_SUCCESS,
    POST_UNBOOKMARK_FAILURE,
    POST_CURSOR,
} from '../actions/postActions'

const initState = {
    postOpen: false,
    postText: '',
    cursor: 0,
    posts: [],
    saved: [],
    pending: false,
    error: null
}

export const postReducer = (state = initState, action) => {
    switch (action.type) {
        // Edit
        case POST_WRITE:
            return {
                ...state,
                postText: action.text
            }
        // Cursor
        case POST_CURSOR:
            return {
                ...state,
                cursor: action.position
            }
        // Open and close modal
        case 'POST_OPEN':
            return {
                ...state,
                postOpen: true,
                postText: action.text
            }
        case 'POST_CLOSE':
            return {
                ...state,
                postOpen: false,
                postText: ''
            }
        // Create post
        case POST_CREATE_PENDING:
            return {
                ...state,
                pending: true
            }
        case POST_CREATE_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: [action.post, ...state.posts]
            }
        case POST_CREATE_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        // Create comment
        case COMMENT_CREATE_PENDING:
            return {
                ...state,
                pending: true
            }
        case COMMENT_CREATE_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.map(post => post._id === action.postId ?
                    { ...post, comments: [...post.comments, action.comment] }
                    : post)
            }
        case COMMENT_CREATE_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        // Delete comment
        case COMMENT_DELETE_PENDING:
            return {
                ...state,
                pending: true
            }
        case COMMENT_DELETE_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.map(post => post._id === action.postId ?
                    { ...post, comments: post.comments.filter(comment => comment._id !== action.commentId) }
                    : post)
            }
        case COMMENT_DELETE_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        // Delete post
        case POST_DELETE_PENDING:
            return {
                ...state,
                pending: true
            }
        case POST_DELETE_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.filter(post => post._id !== action.post._id)
            }
        case POST_DELETE_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        // Get latest posts
        case POST_GET_PENDING:
            return {
                ...state,
                pending: true
            }
        case POST_GET_SUCCESS:
            if (action.page === 0) {
                return {
                    ...state,
                    pending: false,
                    posts: [...action.posts]
                }
            }
            return {
                ...state,
                pending: false,
                posts: [...state.posts, ...action.posts]
            }
        case POST_GET_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        // Heart post
        case POST_HEART_PENDING:
            return {
                ...state,
                pending: true
            }
        case POST_HEART_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.map(post =>
                    post._id === action.postId ? { ...post, hearts: [...post.hearts, action.user] } : post
                )
            }
        case POST_HEART_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }

        // Unheart post
        case POST_UNHEART_PENDING:
            return {
                ...state,
                pending: true
            }
        case POST_UNHEART_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.map(post =>
                    post._id === action.postId ? { ...post, hearts: post.hearts.filter(x => x !== action.user) } : post
                )
            }
        case POST_UNHEART_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        // Save post user
        case POST_BOOKMARK_PENDING:
            return {
                ...state,
            }
        case POST_BOOKMARK_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.map(post =>
                    post._id === action.id ? { ...post, saves: [...post.saves, action.user] } : post
                )
            }
        case POST_BOOKMARK_FAILURE:
            return {
                ...state,
                error: action.error
            }
        // Unsave post
        case POST_UNBOOKMARK_PENDING:
            return {
                ...state,
            }
        case POST_UNBOOKMARK_SUCCESS:
            return {
                ...state,
                pending: false,
                posts: state.posts.map(post =>
                    post._id === action.id ? { ...post, saves: post.saves.filter(x => x !== action.user) } : post
                )
            }
        case POST_UNBOOKMARK_FAILURE:
            return {
                ...state,
                error: action.error
            }
        default:
            return state;
    }
}