import { SETUP_OPEN, SETUP_CLOSE } from '../actions/appActions'

const initialState = {
    setupModal: false,
    createModal: false,
    profile: null,
    pending: false,
    error: null
}

export const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case SETUP_OPEN:
            return {
                ...state,
                setupModal: true
            }
        case SETUP_CLOSE:
            return {
                ...state,
                setupModal: false
            }
        default:
            return state;
    }
}
