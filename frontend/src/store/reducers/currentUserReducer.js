import {
  REGISTER_USER_PENDING,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  LOGIN_USER_PENDING,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  LOGOUT_USER_PENDING,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILURE,
  REFRESH_TOKEN_PENDING,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE,
  UPDATE_USER_PENDING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
} from '../actions/currentUserActions'
import {
  FOLLOW_USER_PENDING,
  FOLLOW_USER_SUCCESS,
  FOLLOW_USER_FAILURE,
  UNFOLLOW_USER_PENDING,
  UNFOLLOW_USER_SUCCESS,
  UNFOLLOW_USER_FAILURE
} from '../actions/userActions'

const initialState = {
  pending: false,
  user: null,
  error: null
}


export const currentUserReducer = (state = initialState, action) => {
  switch (action.type) {
    // Register
    case REGISTER_USER_PENDING:
      return {
        ...state,
        pending: true
      }
    case REGISTER_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        user: action.user
      }
    case REGISTER_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }

    // Login
    case LOGIN_USER_PENDING:
      return {
        ...state,
        pending: true
      }
    case LOGIN_USER_SUCCESS:
      localStorage.setItem('accessToken', action.tokens.accessToken)
      localStorage.setItem('refreshToken', action.tokens.refreshToken)
      return {
        ...state,
        pending: false,
        user: action.user
      }
    case LOGIN_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }

    // Logout
    case LOGOUT_USER_PENDING:
      return {
        ...state,
        pending: true
      }
    case LOGOUT_USER_SUCCESS:
      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
      return {
        ...state,
        pending: false,
        user: null
      }
    case LOGOUT_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }

    // Refresh
    case REFRESH_TOKEN_PENDING:
      return {
        ...state,
        pending: true
      }
    case REFRESH_TOKEN_SUCCESS:
      localStorage.setItem('accessToken', action.tokens.accessToken)
      return {
        ...state,
        pending: false,
        user: action.user
      }
    case REFRESH_TOKEN_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }

    // Update
    case UPDATE_USER_PENDING:
      return {
        ...state,
        pending: true
      }
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        user: action.user
      }
    case UPDATE_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }

    // Follow user
    case FOLLOW_USER_PENDING:
      return {
        ...state,
        pending: true
      }
    case FOLLOW_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        user: {
          ...state.user,
          following: [...state.user.following, action.following]
        }
      }
    case FOLLOW_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }

    // unfollow user
    case UNFOLLOW_USER_PENDING:
      return {
        ...state,
        pending: true
      }
    case UNFOLLOW_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        user: {
          ...state.user,
          following: unfollow(state, action)
        }
      }
    case UNFOLLOW_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      }
    default:
      return state;
  }
}

const unfollow = (state, action) => {
  return state.user.following.filter(x => x !== action.following)
}

export const getUser = state => state.user
export const getUserPending = state => state.pending
export const getUserError = state => state.error
