import { createChatPending, createChatSuccess, createChatFailure } from '../../actions/chatActions'
import refreshToken from '../auth/refreshToken'
import history from '../../../helpers/history'


const createChat = (recipient, content) => {
    return dispatch => {
        dispatch(createChatPending())
        fetch(`/messaging/create`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify({ recipient, text: content.text || '', image: content.image || '' }),
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(createChatSuccess(res.data))
                    history.push(`/messages/${recipient}`)
                } else {
                    dispatch(createChatFailure())
                }
            })
            .catch(error => {
                dispatch(createChatFailure(error))
                //dispatch(refreshToken(createChat, { recipient, content }))
            })
    }
}

export default createChat