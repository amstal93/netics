import { postCreatePending, postCreateSuccess, postCreateFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'


const createPost = (formValues) => {
    return dispatch => {
        dispatch(postCreatePending())
        fetch(`/api/v1/post`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify(formValues),
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postCreateSuccess(res.data))
                } else {
                    dispatch(postCreateFailure())
                    dispatch(refreshToken(createPost, { formValues }))
                }
            })
            .catch(error => {
                dispatch(postCreateFailure(error))
            })
    }
}

export default createPost