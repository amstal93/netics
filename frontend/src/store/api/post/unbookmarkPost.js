import { postUnbookmarkPending, postUnbookmarkSuccess, postUnbookmarkFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'

const bookmarkPost = (postId, user) => {
    return dispatch => {
        dispatch(postUnbookmarkPending())
        fetch(`/api/v1/unsave?id=${postId}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                }
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postUnbookmarkSuccess(postId, user))
                } else {
                    dispatch(refreshToken(bookmarkPost, { postId, user }))
                }
            })
            .catch(error => {
                dispatch(postUnbookmarkFailure(error))
            })
    }
}

export default bookmarkPost