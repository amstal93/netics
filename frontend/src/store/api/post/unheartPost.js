import { postUnheartPending, postUnheartSuccess, postUnheartFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'


const createPost = (postId, user) => {
    return dispatch => {
        dispatch(postUnheartPending())
        fetch(`/api/v1/heart?id=${postId}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postUnheartSuccess(postId, user))
                } else {
                    dispatch(postUnheartFailure())
                    dispatch(refreshToken(createPost, { postId, user }))
                }
            })
            .catch(error => {
                dispatch(postUnheartFailure(error))
            })
    }
}

export default createPost