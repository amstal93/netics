import { postBookmarkPending, postBookmarkSuccess, postBookmarkFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'

const bookmarkPost = (postId, user) => {
    return dispatch => {
        dispatch(postBookmarkPending())
        fetch(`/api/v1/save?id=${postId}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                }
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postBookmarkSuccess(postId, user))
                } else {
                    dispatch(postBookmarkFailure())
                    dispatch(refreshToken(bookmarkPost, { postId, user }))
                }
            })
            .catch(error => {
                dispatch(postBookmarkFailure(error))
            })
    }
}

export default bookmarkPost