import { commentCreatePending, commentCreateSuccess, commentCreateFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'


const createPost = (postId, comment) => {
    return dispatch => {
        dispatch(commentCreatePending())
        fetch(`/api/v1/comment?id=${postId}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify(comment),
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(commentCreateSuccess(postId, res.data))
                } else {
                    dispatch(commentCreateFailure())
                    dispatch(refreshToken(createPost, {postId, comment}))
                }
            })
            .catch(error => {
                dispatch(commentCreateFailure(error))
            })
    }
}

export default createPost