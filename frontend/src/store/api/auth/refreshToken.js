import { refreshTokenPending, refreshTokenSuccess, refreshTokenFailure } from '../../actions/currentUserActions'
import logoutUser from './logoutUser'

const refreshToken = (callback, params) => {
    return dispatch => {
        dispatch(refreshTokenPending())
        fetch(`/api/auth/refresh`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('refreshToken')}`,
                    Accept: 'application/json',
                },
                method: 'POST',
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(refreshTokenSuccess(res.data, res.tokens))
                    if (callback !== undefined && params !== undefined) {
                        callback(...params)
                    }
                } else {
                    dispatch(refreshTokenFailure(res))
                    dispatch(logoutUser())
                }
            })
            .catch(error => {
                dispatch(refreshTokenFailure(error))
                dispatch(logoutUser())
            })
    }
}

export default refreshToken