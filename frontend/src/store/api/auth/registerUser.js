import { registerUserPending, registerUserSuccess, registerUserFailure } from '../../actions/currentUserActions'
import {
  notification,
  Icon
} from 'antd';
import React from 'react'
import loginUser from './loginUser';
import { setupOpen } from '../../actions/appActions';

const errorNotification = (message) => {
  notification.error({
    message: 'Yikes!',
    description: message,
    icon: <Icon theme="twoTone" type="warning" twoToneColor="#FF0000" />,
  });
}

const successNotification = (message) => {
  notification.success({
    message: 'Welcome!',
    description: message,
    icon: <Icon theme="twoTone" type="smile" twoToneColor="#00FF00" />,
  });
}

const registerUser = (formValues) => {
  return dispatch => {
    dispatch(registerUserPending())
    fetch(`/api/auth/register`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        },
        body: JSON.stringify(formValues),
      }).then(res => res.json())
      .then(res => {
        if (res.error) {
          throw (res.error)
        } else if (res.ok) {
          dispatch(registerUserSuccess())
          dispatch(loginUser({ 'username': formValues.username, password: formValues.password }))
          dispatch(setupOpen())
          successNotification(res.message)
        } else {
          dispatch(registerUserFailure(res))
          errorNotification(res.message)
        }
      })
      .catch(error => {
        dispatch(registerUserFailure(error))
        errorNotification(error.message)
      })
  }
}

export default registerUser