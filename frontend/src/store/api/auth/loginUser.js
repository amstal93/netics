import { loginUserPending, loginUserSuccess, loginUserFailure } from '../../actions/currentUserActions'
import history from '../../../helpers/history'
import {
    notification,
    Icon
} from 'antd';
import React from 'react'

const errorNotification = (message) => {
    notification.error({
        message: 'Yikes!',
        description: message,
        icon: <Icon theme="twoTone" type="warning" twoToneColor="#FF0000" />,
    });
}

const loginUser = (credentials) => {
    return dispatch => {
        dispatch(loginUserPending())
        fetch(`/api/auth/login`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify(credentials),
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {

                    dispatch(loginUserSuccess(res.data, res.tokens))
                    window.location.replace('/')
                } else {
                    dispatch(loginUserFailure(res))
                    errorNotification(res.message)
                }
            })
            .catch(error => {
                dispatch(loginUserFailure(error))
                errorNotification(error.message)
            })
    }
}

export default loginUser