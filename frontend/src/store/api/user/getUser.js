import { getUserPending, getUserSuccess, getUserFailure } from '../../actions/userActions'
import refreshToken from '../auth/refreshToken'


const getUser = (args) => {
    return dispatch => {
        dispatch(getUserPending())
        fetch(`/api/v1/user?${args}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(getUserSuccess(res.data))
                } else {
                    dispatch(getUserFailure(res))
                    dispatch(refreshToken(getUser, { args }))
                }
            })
            .catch(error => {
                dispatch(getUserFailure(error))
            })
    }
}

export default getUser