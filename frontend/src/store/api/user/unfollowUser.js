import { unfollowUserPending, unfollowUserSuccess, unfollowUserFailure } from '../../actions/userActions'
import refreshToken from '../auth/refreshToken'


const unfollowUser = (follower, following) => {
    return dispatch => {
        dispatch(unfollowUserPending())
        fetch(`/api/v1/unfollow?user=${follower}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(unfollowUserSuccess(follower, following))
                } else {
                    dispatch(unfollowUserFailure(res))
                    dispatch(refreshToken(unfollowUser, { follower, following }))
                }
            })
            .catch(error => {
                dispatch(unfollowUserFailure(error))
            })
    }
}

export default unfollowUser