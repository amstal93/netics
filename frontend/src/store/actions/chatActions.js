export const sendMessage = (message, chatId) => ({
    type: 'SEND_MESSAGE',
    message,
    chatId
})

export const fetchChat = (message, recipient) => ({
    type: 'FETCH_CHAT',
    message,
    recipient
})

export const createChat = (message, recipient) => ({
    type: 'CREATE_CHAT',
    message,
    recipient
})

export const pushMsg = (message) => ({
    type: 'PUSH_MSG',
    message
})

export const pushChat = (chat) => ({
    type: 'PUSH_CHAT',
    chat
})

export const pushUsers = (users) => ({
    type: 'PUSH_USERS',
    users
})


export const PUSH_MSG = 'PUSH_MSG'
export const PUSH_CHAT = 'PUSH_CHAT'
export const PUSH_USERS = 'PUSH_USERS'

export const SEND_MESSAGE_PENDING = 'SEND_MESSAGE_PENDING'
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS'
export const SEND_MESSAGE_FAILURE = 'SEND_MESSAGE_FAILURE'

export const CREATE_CHAT_PENDING = 'CREATE_CHAT_PENDING'
export const CREATE_CHAT_SUCCESS = 'CREATE_CHAT_SUCCESS'
export const CREATE_CHAT_FAILURE = 'CREATE_CHAT_FAILURE'

export const FETCH_CHAT_PENDING = 'FETCH_CHAT_PENDING'
export const FETCH_CHAT_SUCCESS = 'FETCH_CHAT_SUCCESS'
export const FETCH_CHAT_FAILURE = 'FETCH_CHAT_FAILURE'

export const sendMessagePending = () => {
    return {
        type: SEND_MESSAGE_PENDING
    }
}

export const sendMessageSuccess = (chatId, message) => {
    return {
        type: SEND_MESSAGE_SUCCESS,
        message,
        chatId
    }
}

export const sendMessageFailure = (error) => {
    return {
        type: SEND_MESSAGE_FAILURE,
        error
    }
}


export const createChatPending = () => {
    return {
        type: CREATE_CHAT_PENDING
    }
}

export const createChatSuccess = (chat) => {
    return {
        type: CREATE_CHAT_SUCCESS,
        chat
    }
}

export const createChatFailure = (error) => {
    return {
        type: CREATE_CHAT_FAILURE,
        error
    }
}

export const fetchChatPending = () => {
    return {
        type: FETCH_CHAT_PENDING
    }
}

export const fetchChatSuccess = (data) => {
    return {
        type: FETCH_CHAT_SUCCESS,
        data
    }
}

export const fetchChatFailure = (error) => {
    return {
        type: FETCH_CHAT_FAILURE,
        error
    }
}

export const pushMsgFunc = (message) => {
    return {
        type: PUSH_MSG,
        message
    }
}

export const pushChatFunc = (chat) => {
    return {
        type: PUSH_CHAT,
        chat
    }
}
