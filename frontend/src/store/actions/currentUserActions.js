export const register = (formValues) => ({
    type: 'REGISTER',
    formValues
})

export const login = (credentials) => ({
    type: 'LOGIN',
    credentials
})

export const refresh = () => ({
    type: 'REFRESH'
})

export const logout = () => ({
    type: 'LOGOUT'
})

export const update = (user) => ({
    type: 'UPDATE',
    user
})

export const REGISTER_USER_PENDING = 'REGISTER_USER_PENDING'
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS'
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE'

export const LOGIN_USER_PENDING = 'LOGIN_USER_PENDING'
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS'
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE'

export const LOGOUT_USER_PENDING = 'LOGOUT_USER_PENDING'
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS'
export const LOGOUT_USER_FAILURE = 'LOGOUT_USER_FAILURE'

export const REFRESH_TOKEN_PENDING = 'REFRESH_TOKEN_PENDING'
export const REFRESH_TOKEN_SUCCESS = 'REFRESH_TOKEN_SUCCESS'
export const REFRESH_TOKEN_FAILURE = 'REFRESH_TOKEN_FAILURE'

export const UPDATE_USER_PENDING = 'UPDATE_USER_PENDING'
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS'
export const UPDATE_USER_FAILURE = 'UPDATE_USER_FAILURE'

export const registerUserPending = () => {
    return {
        type: REGISTER_USER_PENDING
    }
}

export const registerUserSuccess = (user, tokens) => {
    return {
        type: REGISTER_USER_SUCCESS,
        user,
        tokens
    }
}

export const registerUserFailure = (error) => {
    return {
        type: REGISTER_USER_FAILURE,
        error
    }
}

export const loginUserPending = () => {
    return {
        type: LOGIN_USER_PENDING
    }
}

export const loginUserSuccess = (user, tokens) => {
    return {
        type: LOGIN_USER_SUCCESS,
        user,
        tokens
    }
}

export const loginUserFailure = (error) => {
    return {
        type: LOGIN_USER_FAILURE,
        error
    }
}

export const logoutUserPending = () => {
    return {
        type: LOGOUT_USER_PENDING
    }
}

export const logoutUserSuccess = (user) => {
    return {
        type: LOGOUT_USER_SUCCESS
    }
}

export const logoutUserFailure = (error) => {
    return {
        type: LOGOUT_USER_FAILURE,
        error
    }
}

export const refreshTokenPending = () => {
    return {
        type: REFRESH_TOKEN_PENDING
    }
}

export const refreshTokenSuccess = (user, tokens) => {
    return {
        type: REFRESH_TOKEN_SUCCESS,
        user,
        tokens
    }
}

export const refreshTokenFailure = (error) => {
    return {
        type: REFRESH_TOKEN_FAILURE,
        error
    }
}

export const updateUserPending = () => {
    return {
        type: UPDATE_USER_PENDING
    }
}

export const updateUserSuccess = (user) => {
    return {
        type: UPDATE_USER_SUCCESS,
        user
    }
}

export const updateUserFailure = (error) => {
    return {
        type: UPDATE_USER_FAILURE,
        error
    }
}
