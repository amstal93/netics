export const postCreate = () => ({
    type: 'POST_CREATE',
})

export const postComment = () => ({
    type: 'COMMENT_CREATE',
})

export const deleteComment = () => ({
    type: 'COMMENT_DELETE',
})

export const postDelete = () => ({
    type: 'POST_DELETE',
})

export const postOpen = (text) => ({
    type: 'POST_OPEN',
    text
})

export const postWrite = (text) => ({
    type: 'POST_WRITE',
    text
})

export const postCursor = (position) => ({
    type: 'POST_CURSOR',
    position
})

export const postClose = () => ({
    type: 'POST_CLOSE'
})

export const postGet = () => ({
    type: 'POST_GET',
})

export const postHeart = () => ({
    type: 'POST_HEART',
})

export const postUnheart = () => ({
    type: 'POST_UNHEART',
})

export const postBookmark = () => ({
    type: 'POST_BOOKMARK',
})

export const postUnbookmark = () => ({
    type: 'POST_UNBOOKMARK',
})

export const POST_WRITE = 'POST_WRITE'
export const POST_CURSOR = 'POST_CURSOR'

export const COMMENT_CREATE_PENDING = 'COMMENT_CREATE_PENDING'
export const COMMENT_CREATE_SUCCESS = 'COMMENT_CREATE_SUCCESS'
export const COMMENT_CREATE_FAILURE = 'COMMENT_CREATE_FAILURE'

export const POST_BOOKMARK_PENDING = 'POST_BOOKMARK_PENDING'
export const POST_BOOKMARK_SUCCESS = 'POST_BOOKMARK_SUCCESS'
export const POST_BOOKMARK_FAILURE = 'POST_BOOKMARK_FAILURE'

export const POST_UNBOOKMARK_PENDING = 'POST_UNBOOKMARK_PENDING'
export const POST_UNBOOKMARK_SUCCESS = 'POST_UNBOOKMARK_SUCCESS'
export const POST_UNBOOKMARK_FAILURE = 'POST_UNBOOKMARK_FAILURE'

export const COMMENT_DELETE_PENDING = 'COMMENT_DELETE_PENDING'
export const COMMENT_DELETE_SUCCESS = 'COMMENT_DELETE_SUCCESS'
export const COMMENT_DELETE_FAILURE = 'COMMENT_DELETE_FAILURE'

export const POST_CREATE_PENDING = 'POST_CREATE_PENDING'
export const POST_CREATE_SUCCESS = 'POST_CREATE_SUCCESS'
export const POST_CREATE_FAILURE = 'POST_CREATE_FAILURE'

export const POST_GET_PENDING = 'POST_GET_PENDING'
export const POST_GET_SUCCESS = 'POST_GET_SUCCESS'
export const POST_GET_FAILURE = 'POST_GET_FAILURE'

export const POST_DELETE_PENDING = 'POST_DELETE_PENDING'
export const POST_DELETE_SUCCESS = 'POST_DELETE_SUCCESS'
export const POST_DELETE_FAILURE = 'POST_DELETE_FAILURE'

export const POST_HEART_PENDING = 'POST_HEART_PENDING'
export const POST_HEART_SUCCESS = 'POST_HEART_SUCCESS'
export const POST_HEART_FAILURE = 'POST_HEART_FAILURE'

export const POST_UNHEART_PENDING = 'POST_UNHEART_PENDING'
export const POST_UNHEART_SUCCESS = 'POST_UNHEART_SUCCESS'
export const POST_UNHEART_FAILURE = 'POST_UNHEART_FAILURE'

export const postCreatePending = () => {
    return {
        type: POST_CREATE_PENDING
    }
}

export const postCreateSuccess = (post) => {
    return {
        type: POST_CREATE_SUCCESS,
        post
    }
}

export const postCreateFailure = (error) => {
    return {
        type: POST_CREATE_FAILURE,
        error
    }
}

export const postBookmarkPending = () => {
    return {
        type: POST_BOOKMARK_PENDING
    }
}

export const postBookmarkSuccess = (id, user) => {
    return {
        type: POST_BOOKMARK_SUCCESS,
        id,
        user
    }
}

export const postBookmarkFailure = (error) => {
    return {
        type: POST_BOOKMARK_FAILURE,
        error
    }
}

export const postUnbookmarkPending = () => {
    return {
        type: POST_UNBOOKMARK_PENDING
    }
}

export const postUnbookmarkSuccess = (id, user) => {
    return {
        type: POST_UNBOOKMARK_SUCCESS,
        id,
        user
    }
}

export const postUnbookmarkFailure = (error) => {
    return {
        type: POST_UNBOOKMARK_FAILURE,
        error
    }
}

export const commentCreatePending = () => {
    return {
        type: COMMENT_CREATE_PENDING
    }
}

export const commentCreateSuccess = (postId, comment) => {
    return {
        type: COMMENT_CREATE_SUCCESS,
        comment,
        postId
    }
}

export const commentCreateFailure = (error) => {
    return {
        type: COMMENT_CREATE_FAILURE,
        error
    }
}

export const commentDeletePending = () => {
    return {
        type: COMMENT_DELETE_PENDING
    }
}

export const commentDeleteSuccess = (postId, commentId) => {
    return {
        type: COMMENT_DELETE_SUCCESS,
        commentId,
        postId
    }
}

export const commentDeleteFailure = (error) => {
    return {
        type: COMMENT_DELETE_FAILURE,
        error
    }
}

export const postGetPending = () => {
    return {
        type: POST_GET_PENDING
    }
}

export const postGetSuccess = (posts, page) => {
    return {
        type: POST_GET_SUCCESS,
        posts,
        page
    }
}

export const postGetFailure = (error) => {
    return {
        type: POST_GET_FAILURE,
        error
    }
}

export const postDeletePending = () => {
    return {
        type: POST_DELETE_PENDING
    }
}

export const postDeleteSuccess = (post) => {
    return {
        type: POST_DELETE_SUCCESS,
        post
    }
}

export const postDeleteFailure = (error) => {
    return {
        type: POST_DELETE_FAILURE,
        error
    }
}

export const postHeartPending = () => {
    return {
        type: POST_HEART_PENDING
    }
}

export const postHeartSuccess = (postId, user) => {
    return {
        type: POST_HEART_SUCCESS,
        postId,
        user
    }
}

export const postHeartFailure = (error) => {
    return {
        type: POST_HEART_FAILURE,
        error
    }
}

export const postUnheartPending = () => {
    return {
        type: POST_UNHEART_PENDING
    }
}

export const postUnheartSuccess = (postId, user) => {
    return {
        type: POST_UNHEART_SUCCESS,
        postId,
        user
    }
}

export const postUnheartFailure = (error) => {
    return {
        type: POST_UNHEART_FAILURE,
        error
    }
}