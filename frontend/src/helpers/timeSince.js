const timeSince = (time) => {
    const now = new Date()
    const since = new Date(time)
    if (now.getUTCFullYear() - since.getUTCFullYear() > 0) {
        return `${now.getUTCFullYear() - since.getUTCFullYear()}Y`
    } else if (now.getUTCMonth() - since.getUTCMonth() > 0) {
        return `${now.getUTCMonth() - since.getUTCMonth()}M`
    } else if (now.getUTCDay() - since.getUTCDay() > 0) {
        return `${now.getUTCDay() - since.getUTCDay()}D`
    } else if (now.getUTCHours() - since.getUTCHours() > 0) {
        return `${now.getUTCHours() - since.getUTCHours()}h`
    } else if (now.getUTCMinutes() - since.getUTCMinutes() > 0) {
        return `${now.getUTCMinutes() - since.getUTCMinutes()}m`
    } else if (now.getUTCSeconds() - since.getUTCSeconds() > 0) {
        return `${now.getUTCSeconds() - since.getUTCSeconds()}s`
    }
    return ''
}

export default timeSince